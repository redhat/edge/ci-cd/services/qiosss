FROM quay.io/automotive-toolchain/python3-pdm:latest@sha256:c144489a0dfb817bf2db5a82bdeb917e5f8bfbbf090e12fa29d71f8882a66f1b as builder

WORKDIR /qiosss
COPY pyproject.toml pdm.lock README.md ./
COPY src src

RUN pdm build --no-sdist \
              -d ./build \
 && pdm export --prod \
               --without-hashes \
               -f requirements \
 > ./build/requirements.txt

FROM registry.access.redhat.com/ubi9/ubi:9.3-1552@sha256:1fafb0905264413501df60d90a92ca32df8a2011cbfb4876ddff5ceb20c8f165

ENV DNF_OPTS="--setopt=install_weak_deps=False --setopt=tsflags=nodocs"
RUN dnf install -y python3-devel git \
 && dnf clean all -y
COPY --from=builder /qiosss/build/*.whl /qiosss/build/requirements.txt /tmp/

RUN pip install --ignore-installed \
                --no-deps \
                -r /tmp/requirements.txt \
 && pip install --no-cache-dir \
                --no-deps \
                /tmp/*.whl \
 && rm /tmp/*.whl /tmp/requirements.txt \

