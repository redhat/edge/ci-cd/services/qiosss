#!/usr/bin/env python3

import argparse
import logging
import os
import sys

from .containers import get_container_images
from .reports import generate_reports
from .notifications import send_notification_slack


def argparser() -> argparse.ArgumentParser:
    """Argparser configuration for qiosss"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--exclude-tags",
        nargs="+",
        help="List of container image tags to exclude from the reports. Use the format '<image-name>:<tag-name> <image-name>:<tag-name> ...'",
    )
    parser.add_argument(
        "--exclude-severity",
        nargs="+",
        choices=["Critical", "High", "Medium", "Low"],
        help="List of CVE severity levels to exclude from the reports",
    )
    return parser


def main() -> None:
    """Entrypoint for qiosss"""
    parser = argparser()
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    # Assert needed Quay parameters are set via environment variables
    quay_parameters = {"QUAY_NAMESPACE": None, "QUAY_APP_AUTH_TOKEN": None}
    for quay_parameter in quay_parameters:
        quay_parameter_value = os.getenv(quay_parameter)
        if not quay_parameter_value:
            logging.error("%s not set.", quay_parameter)

        quay_parameters[quay_parameter] = quay_parameter_value

    if any(param is None for param in quay_parameters.values()):
        sys.exit(1)

    logging.info("Running qiosss")

    # Get list of container images in CONTAINER_REGISTRY_NAMESPACE and tags
    # that do not expire, and associated vulnerabilities
    logging.info("Gathering container data")
    container_images = get_container_images(
        quay_parameters,
        exclude_tags=args.exclude_tags,
        exclude_severity=args.exclude_severity,
    )
    logging.info("Container data gathered")

    # Generate report data using container_images
    logging.info("Writing reports")
    generate_reports(container_images)
    logging.info("Reports generated")

    # Send slack message
    logging.info("Sending slack notifications")
    send_notification_slack(container_images)
    logging.info("Slack notifications sent")


if __name__ == "__main__":
    main()
