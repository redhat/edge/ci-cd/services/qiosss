import unittest
from unittest.mock import patch, MagicMock
from src.qiosss.containers import (
    get_container_vulnerabilities,
    get_container_image_tags,
    get_container_images,
    is_image_tag_excluded,
    ContainerImage,
    Vulnerability,
)


class TestContainers(unittest.TestCase):
    @patch("src.qiosss.containers.requests.get")
    def test_get_container_vulnerabilities(self, mock_requests_get):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "status": "scanned",
            "data": {
                "Layer": {
                    "Features": [
                        {
                            "Vulnerabilities": [
                                {
                                    "Name": "CVE-2021-1234",
                                    "Severity": "High",
                                    "FixedBy": True,
                                }
                            ]
                        },
                        {
                            "Vulnerabilities": [
                                {
                                    "Name": "CVE-2022-5678",
                                    "Severity": "Critical",
                                    "FixedBy": False,
                                }
                            ]
                        },
                        {
                            "Vulnerabilities": [
                                {
                                    "Name": "CVE-2023-9876",
                                    "Severity": "Medium",
                                    "FixedBy": True,
                                }
                            ]
                        },
                    ]
                }
            },
        }
        mock_requests_get.return_value = mock_response

        vulnerabilities = get_container_vulnerabilities(
            {}, "image_name", "digest", exclude_severity=["High"]
        )

        self.assertEqual(len(vulnerabilities), 2)
        self.assertEqual(mock_requests_get.call_count, 1)
        self.assertEqual(
            vulnerabilities[0],
            Vulnerability(cve="CVE-2022-5678", severity="Critical", fixable=False),
        )
        self.assertEqual(
            vulnerabilities[1],
            Vulnerability(cve="CVE-2023-9876", severity="Medium", fixable=True),
        )

    def test_is_image_tag_excluded(self):
        self.assertFalse(is_image_tag_excluded(None, "image_name", "tag_name"))
        self.assertFalse(
            is_image_tag_excluded(
                ["exclude_image:exclude_tag"], "image_name", "tag_name"
            )
        )
        self.assertTrue(
            is_image_tag_excluded(["image_name:tag_name"], "image_name", "tag_name")
        )

    @patch("src.qiosss.containers.requests.get")
    def test_get_container_image_tags(self, mock_requests_get):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "tags": [{"name": "tag1", "manifest_digest": "digest1"}]
        }
        mock_requests_get.return_value = mock_response

        tags = get_container_image_tags({}, "image_name")

        self.assertEqual(len(tags), 1)
        self.assertEqual(mock_requests_get.call_count, 1)

    @patch("src.qiosss.containers.requests.get")
    @patch("src.qiosss.containers.get_container_vulnerabilities")
    def test_get_container_images(
        self, mock_get_container_vulnerabilities, mock_requests_get
    ):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            "repositories": [{"name": "repo1"}, {"name": "repo2"}]
        }
        mock_requests_get.return_value = mock_response

        quay_parameters = {
            "QUAY_NAMESPACE": "namespace",
            "QUAY_APP_AUTH_TOKEN": "token",
        }

        with patch(
            "src.qiosss.containers.get_container_image_tags"
        ) as mock_get_container_image_tags:
            mock_get_container_image_tags.return_value = {"tag1": "digest1"}

            mock_get_container_vulnerabilities.return_value = []

            container_images = get_container_images(quay_parameters)

        self.assertEqual(len(container_images), 2)
        self.assertIsInstance(container_images[0], ContainerImage)
        self.assertIsInstance(container_images[1], ContainerImage)
        mock_get_container_vulnerabilities.assert_called()


if __name__ == "__main__":
    unittest.main()
