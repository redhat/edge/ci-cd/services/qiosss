#!/usr/bin/env python3

from typing import Collection
from .containers import ContainerImage


def generate_reports(container_images: Collection[ContainerImage]) -> None:
    for container_image in container_images:
        # Check if the container has vulnerabilities
        if not container_image.vulnerabilities:
            print(
                f"No vulnerabilities found for {container_image.namespace}/{container_image.name}. Skipping report creation."
            )
            continue

        report_filename = (
            f"{container_image.namespace}_{container_image.name}_reports.txt"
        )
        with open(report_filename, "a") as report_file:
            report_file.write(
                f"Container Image: {container_image.namespace}/{container_image.name}\n\n"
            )
            for vulnerability in container_image.vulnerabilities:
                report_file.write(f"CVE: {vulnerability.cve}\n")
                report_file.write(f"Severity: {vulnerability.severity}\n")
                report_file.write(
                    f"Fixable: {'Yes' if vulnerability.fixable else 'No'}\n"
                )
                report_file.write("\n" + "-" * 20 + "\n")
        print(
            f"Report appended for {container_image.namespace}/{container_image.name} in {report_filename}"
        )

    summary_filename = "summary_report.txt"
    with open(summary_filename, "a") as summary_file:
        summary_file.write("Summary Report for Container Images\n\n")
        for container_image in container_images:
            # Check if the container has vulnerabilities
            if not container_image.vulnerabilities:
                continue

            total_vulnerabilities = len(container_image.vulnerabilities)
            summary_file.write(
                f"Container Image: {container_image.namespace}/{container_image.name}\n"
            )
            summary_file.write(f"Total Vulnerabilities: {total_vulnerabilities}\n\n")

    print(f"Summary report appended in {summary_filename}")
