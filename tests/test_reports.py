import unittest
from unittest.mock import mock_open, patch, call  # Import call

from src.qiosss.reports import generate_reports
from src.qiosss.containers import ContainerImage, Vulnerability


class TestGenerateReports(unittest.TestCase):
    def test_generate_reports(self):
        # Create a mock ContainerImage for testing
        container_image = ContainerImage(
            namespace="test_namespace",
            name="test_image",
            tags={},
            vulnerabilities=[
                Vulnerability(cve="CVE-1234", severity="High", fixable=True),
                Vulnerability(cve="CVE-5678", severity="Medium", fixable=False),
            ],
        )

        # Mock the open function and write to the mock file
        with patch("builtins.open", mock_open()) as mock_file:
            generate_reports([container_image])

            # Assert that the file is opened with the correct filename
            expected_calls = [
                call("test_namespace_test_image_reports.txt", "a"),
                call().__enter__(),
                call().write("Container Image: test_namespace/test_image\n\n"),
                call().write("CVE: CVE-1234\n"),
                call().write("Severity: High\n"),
                call().write("Fixable: Yes\n"),
                call().write("\n--------------------\n"),
                call().write("CVE: CVE-5678\n"),
                call().write("Severity: Medium\n"),
                call().write("Fixable: No\n"),
                call().write("\n--------------------\n"),
                call().__exit__(None, None, None),
            ]
            mock_file.assert_has_calls(expected_calls, any_order=True)

            # Assert that the summary file is opened and written
            summary_file_calls = [
                call("summary_report.txt", "a"),
                call().__enter__(),
                call().write("Summary Report for Container Images\n\n"),
                call().write("Container Image: test_namespace/test_image\n"),
                call().write("Total Vulnerabilities: 2\n\n"),
                call().__exit__(None, None, None),
            ]
            mock_file.assert_has_calls(summary_file_calls, any_order=True)


if __name__ == "__main__":
    unittest.main()
