import unittest
from unittest.mock import patch, Mock
from src.qiosss.notifications import send_notification_slack
from src.qiosss.containers import ContainerImage, Vulnerability


class TestSendNotificationSlack(unittest.TestCase):
    @patch("src.qiosss.notifications.os.environ.get")
    @patch("src.qiosss.notifications.requests.post")
    def test_send_notification_slack(self, mock_post, mock_getenv):
        # Set environment variables
        mock_getenv.side_effect = lambda key, default=None: {
            "SLACK_WEBHOOK_URL": "mock_webhook_url",
            "SLACK_CHANNEL": "mock_channel",
        }.get(key, default)

        # Create a mock ContainerImage for testing
        container_image = ContainerImage(
            namespace="test_namespace",
            name="mock_image",
            tags={},
            vulnerabilities=[
                Vulnerability(cve="CVE-1234", severity="High", fixable=True),
                Vulnerability(cve="CVE-5678", severity="Medium", fixable=False),
            ],
        )

        # Call the function with the mock data
        send_notification_slack([container_image])

        # Check that requests.post is called with the correct arguments
        mock_post.assert_called_once_with(
            "mock_webhook_url",
            data='{"channel": "mock_channel", "username": "webhookbot", "pretext": "Vulnerability Report Summary:", "text": "There are 1 containers with 2 total CVEs associated.\\n\\nmock_image (1 high, 1 medium)"}',
            headers={"Content-Type": "application/json"},
        )


if __name__ == "__main__":
    unittest.main()
