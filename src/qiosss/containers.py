#!/usr/bin/env python3

from dataclasses import dataclass
from typing import Collection, Mapping
import requests


@dataclass
class Vulnerability:
    cve: str
    severity: str
    fixable: bool


@dataclass
class ContainerImage:
    namespace: str
    name: str
    tags: Mapping[str, str]
    vulnerabilities: Collection[Vulnerability]


def get_container_vulnerabilities(
    quay_parameters: Mapping[str, str],
    container_image: str,
    container_digest: str,
    exclude_severity: list = None,
) -> Collection[Vulnerability]:
    vulnerabilities: Collection[Vulnerability] = []
    quay_namespace = quay_parameters.get("QUAY_NAMESPACE")
    quay_app_auth_token = quay_parameters.get("QUAY_APP_AUTH_TOKEN")

    # Construct the API endpoint URL
    api_url = f"https://quay.io/api/v1/repository/{quay_namespace}/{container_image}/manifest/{container_digest}/security?vulnerabilities=true"
    # Set the headers with the authorization token
    headers = {"Authorization": f"Bearer {quay_app_auth_token}"}

    try:
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()

        if response.status_code == 200:
            # Parse the JSON response
            security_scan_report = response.json()

            # Check if "status" is the expected value and "data" is not None
            if (
                "status" in security_scan_report
                and security_scan_report["status"] == "scanned"
            ):
                if (
                    "data" in security_scan_report
                    and security_scan_report["data"] is not None
                ):
                    features = (
                        security_scan_report.get("data", {})
                        .get("Layer", {})
                        .get("Features", [])
                    )
                    for feature in features:
                        vulnerabilities_data = feature.get("Vulnerabilities", [])
                        for vulnerability_data in vulnerabilities_data:
                            is_fixable = vulnerability_data.get("FixedBy", True)
                            cve = vulnerability_data.get("Name")
                            severity = vulnerability_data.get("Severity")
                            fixable = bool(is_fixable)

                            # Filter by severity
                            if exclude_severity and severity in exclude_severity:
                                print(f"  Excluding CVE: {cve} (Severity: {severity})")
                                continue

                            print(f"    - CVE: {cve}")
                            print(f"      Severity: {severity}")
                            print(f"      Fixable: {fixable}")

                            vulnerabilities.append(
                                Vulnerability(
                                    cve=cve, severity=severity, fixable=fixable
                                )
                            )
                else:
                    print(f"Received 'data' is None for {container_image}.")
            else:
                print(f"Received 'status' is not 'scanned' for {container_image}.")

        else:
            print(
                f"Failed to retrieve vulnerabilities for {container_image}. Status code: {response.status_code}"
            )

    except requests.exceptions.RequestException as e:
        print(f"Failed to retrieve vulnerabilities for {container_image}: {e}")

    return vulnerabilities


def is_image_tag_excluded(exclude_tags, container_image, tag_name):
    if not exclude_tags:
        return False

    for exclude in exclude_tags:
        exclude_image, exclude_tag = exclude.split(":")
        if container_image == exclude_image and tag_name == exclude_tag:
            return True
    return False


def get_container_image_tags(
    quay_parameters: Mapping[str, str],
    container_image: str,
    exclude_expirable: bool = False,
    exclude_tags: list = None,
) -> Mapping[str, str]:
    quay_namespace = quay_parameters.get("QUAY_NAMESPACE")
    quay_app_auth_token = quay_parameters.get("QUAY_APP_AUTH_TOKEN")
    tag_map = {}
    # Construct the URL to fetch tags
    api_url = f"https://quay.io/api/v1/repository/{quay_namespace}/{container_image}/tag/?onlyActiveTags=true"

    headers = {"Authorization": f"Bearer {quay_app_auth_token}"}

    try:
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()

        if response.status_code == 200:
            tag_data = response.json()
            tags = tag_data.get("tags", [])

            for tag in tags:
                tag_name = tag.get("name")
                tag_digest = tag.get("manifest_digest")
                if exclude_expirable and tag.get("expiration"):
                    continue

                if is_image_tag_excluded(exclude_tags, container_image, tag_name):
                    print(f"  Excluding tag: {tag_name}")
                    continue

                tag_map[tag_name] = tag_digest
        else:
            print(
                f"Failed to retrieve tags for {container_image}. Status code: {response.status_code}"
            )
    except requests.exceptions.RequestException as e:
        print(f"Failed to retrieve tags for {container_image}: {e}")
    return tag_map


def get_container_images(
    quay_parameters: dict, exclude_tags: list = None, exclude_severity: list = None
) -> Collection[ContainerImage]:
    container_images: Collection[ContainerImage] = []
    quay_namespace = quay_parameters.get("QUAY_NAMESPACE")
    quay_app_auth_token = quay_parameters.get("QUAY_APP_AUTH_TOKEN")

    # Construct the API endpoint URL
    api_url = f"https://quay.io/api/v1/repository?namespace={quay_namespace}"

    # Set the headers with the authorization token
    headers = {"Authorization": f"Bearer {quay_app_auth_token}"}

    try:
        # Send a GET request to list repositories, but allow exceptions
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()
        data = response.json()
        repositories = [repo["name"] for repo in data.get("repositories", [])]

        for repo in repositories:
            print(f"Repository: {repo}")

            tag_map = get_container_image_tags(
                quay_parameters, repo, exclude_expirable=True, exclude_tags=exclude_tags
            )
            vulnerabilities = []

            for tag_name, tag_digest in tag_map.items():
                print(f"  {tag_name} ({tag_digest})")
                tag_vulnerabilities = get_container_vulnerabilities(
                    quay_parameters, repo, tag_digest, exclude_severity=exclude_severity
                )
                vulnerabilities.extend(tag_vulnerabilities)

            container_images.append(
                ContainerImage(
                    namespace=quay_namespace,
                    name=repo,
                    tags=tag_map,
                    vulnerabilities=vulnerabilities,
                )
            )

    except requests.exceptions.RequestException as e:
        print(f"Failed to retrieve repositories: {e}")

    return container_images
