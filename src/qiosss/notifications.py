#!/usr/bin/env python3

import json
import os
import requests
from .containers import ContainerImage
from typing import Collection


def send_notification_slack(container_images: Collection[ContainerImage]) -> None:
    SLACK_WEBHOOK_URL = os.environ.get("SLACK_WEBHOOK_URL")
    SLACK_CHANNEL = os.environ.get("SLACK_CHANNEL")

    if not SLACK_WEBHOOK_URL or not SLACK_CHANNEL:
        print("SLACK_WEBHOOK_URL and SLACK_CHANNEL environment variables are not set.")
        return

    # Filter out containers with no vulnerabilities
    container_images_with_vulnerabilities = [
        img for img in container_images if img.vulnerabilities
    ]

    total_containers = len(container_images_with_vulnerabilities)

    # Dictionary to store container-level severity counts
    container_severity_counts = {}

    for container_image in container_images_with_vulnerabilities:
        severity_counts = {"Critical": 0, "High": 0, "Medium": 0, "Low": 0}

        # Count the vulnerabilities by severity for each container
        for vulnerability in container_image.vulnerabilities:
            if vulnerability.severity in severity_counts:
                severity_counts[vulnerability.severity] += 1

        # Store the container-level severity counts
        container_severity_counts[container_image.name] = {
            "Critical": severity_counts["Critical"],
            "High": severity_counts["High"],
            "Medium": severity_counts["Medium"],
            "Low": severity_counts["Low"],
        }

    # Container severity messages
    total_vulnerabilities = 0
    container_severity_messages = []
    for container_name, counts in container_severity_counts.items():
        total_vulnerabilities += sum(count for count in counts.values())
        non_zero_counts = {
            severity: count for severity, count in counts.items() if count > 0
        }
        if non_zero_counts:
            container_info = f"{container_name} ({', '.join(f'{count} {severity.lower()}' for severity, count in non_zero_counts.items())})"
            container_severity_messages.append(container_info)

    # Join the container severity messages with line breaks
    container_severity_message = "\n".join(container_severity_messages)

    extra_msg = os.getenv("QIOSSS_NOTIFICATION_EXTRA", "")
    if extra_msg:
        extra_msg += "\n"
    report_message = (
        f"There are {total_containers} containers with "
        f"{total_vulnerabilities} total CVEs associated.\n"
        f"{extra_msg}"
        f"\n{container_severity_message}"
    )
    payload_data = {
        "channel": SLACK_CHANNEL,
        "username": "webhookbot",
        "pretext": "Vulnerability Report Summary:",
        "text": report_message,
    }
    try:
        response = requests.post(
            SLACK_WEBHOOK_URL,
            data=json.dumps(payload_data),
            headers={"Content-Type": "application/json"},
        )
        response.raise_for_status()
        print("Slack notification sent successfully.")
    except requests.exceptions.RequestException as e:
        print(f"Failed to send Slack notification: {e}")
