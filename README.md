# qiosss

A tool to generate a namespace wide security scan summary report from Quay.io
security scans.

# Developer Information

Using pdm, you can quickly run the program like so:

```
pdm install
pdm run qiosss
```

You need the following environment variables configured:

- QUAY_NAMESPACE
- QUAY_APP_AUTH_TOKEN
